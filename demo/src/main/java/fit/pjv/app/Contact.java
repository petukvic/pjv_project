package fit.pjv.app;


public class Contact {

    private final String name;
    private final String surname;
    private final String phoneNum;
    private final String member;

    public Contact(String name, String surname, String phoneNum, String member) {
        this.name = name;
        this.surname = surname;
        this.phoneNum = phoneNum;
        this.member = member;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public String getMember() {
        return member;
    }

    @Override
    public String toString() {
        return name + " " + surname + "                 Number: " + phoneNum + "          Member: " + member;
    }
}
