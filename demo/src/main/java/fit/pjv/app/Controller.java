package fit.pjv.app;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;

import java.net.URL;
import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    ObservableList<String> obList=FXCollections.observableArrayList("Colleague", "Friend", "Family");
    private final SQLDatabase sqlDatabase= new SQLDatabase("jdbc:sqlite:db.sqlite");


    @FXML
    ListView<Contact> listView;

    @FXML
    private ChoiceBox<String> statusBox=new ChoiceBox<>();

    @FXML
    Button addButton, removeButton, editButton;

    ObservableList<Contact> contacts = FXCollections.observableArrayList();
    private Contact selectedContact;
    private boolean edit = false;

    @FXML
    public void addContact() throws SQLException {
        statusBox.setItems(obList);
        Dialog<Contact> dialog = new Dialog<>();
        dialog.setTitle("Contact editor");
        GridPane grid = new GridPane();
        TextField contactName = new TextField(); contactName.setPromptText("Name");
        TextField contactSurname = new TextField(); contactSurname.setPromptText("Surname");
        TextField contactNumber = new TextField(); contactNumber.setPromptText("Phone number");
        grid.add(contactName, 1, 1);
        grid.add(contactSurname, 1, 2);
        grid.add(contactNumber, 1, 3);
        grid.add(statusBox,1,4);
        if (edit && selectedContact != null) {
            contactName.setText(selectedContact.getName());
            contactSurname.setText(selectedContact.getSurname());
            contactNumber.setText(selectedContact.getPhoneNum());
            statusBox.setValue(selectedContact.getMember());
            sqlDatabase.deleteContactFromDb(selectedContact.getPhoneNum());
            contacts.remove(selectedContact);
        }
        ButtonType buttonTypeOk = new ButtonType("Confirm", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
        dialog.getDialogPane().setContent(grid);
        dialog.setResultConverter(new Callback<>() {
            @Override
            public Contact call(ButtonType b) {
                if (b != null) {
                if (contactName.getText().isEmpty()) {
                    Alert errorAlert = new Alert(Alert.AlertType.ERROR);
                    errorAlert.setHeaderText("Input not valid");
                    errorAlert.setContentText("Name is empty");
                    errorAlert.showAndWait();
                    return null;
                }
                if (contactSurname.getText().isEmpty()) {
                    Alert errorAlert = new Alert(Alert.AlertType.ERROR);
                    errorAlert.setHeaderText("Input not valid");
                    errorAlert.setContentText("Surname is empty");
                            errorAlert.showAndWait();
                    return null;
                }
                try {
                    Long.parseLong(contactNumber.getText());
                } catch (Exception e) {
                    Alert errorAlert = new Alert(Alert.AlertType.ERROR);
                    errorAlert.setHeaderText("Input not valid");
                    errorAlert.setContentText("Telephone number should be a number");
                    errorAlert.showAndWait();
                    return null;
                }
                if (statusBox.getValue()==null) {
                        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
                        errorAlert.setHeaderText("Input not valid");
                        errorAlert.setContentText("Box should be filled");
                        errorAlert.showAndWait();
                        return null;
                }
                for (var contact : contacts) {
                    if (contact.getPhoneNum().equals(contactNumber.getText())) {
                        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
                        errorAlert.setHeaderText("Input not valid");
                        errorAlert.setContentText("Such number exists in a database");
                        errorAlert.showAndWait();
                        return null;
                    }
                }

                    try {
                        sqlDatabase.insertContactIntoDb(new Contact(contactName.getText(), contactSurname.getText(), contactNumber.getText(), statusBox.getValue()));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    return new Contact(contactName.getText(), contactSurname.getText(), contactNumber.getText(), statusBox.getValue());
            } return null; }
        });
        Optional<Contact> result = dialog.showAndWait();
        if (result.isPresent()) {
            contacts.add(result.get());
            listView.setItems(contacts);
        }
        edit = false;
    }

    @FXML
    public void removeContact() throws SQLException {
        if (selectedContact != null) {
            sqlDatabase.deleteContactFromDb(selectedContact.getPhoneNum());
            contacts.remove(selectedContact);
            listView.setItems(contacts);
        }
    }

    @FXML
    public void editContact() throws SQLException {
        if (selectedContact != null) {
            this.edit = true;
            addContact();
        }
    }

    public void initialize(URL arg0, ResourceBundle arg1) {
        listView.getSelectionModel().selectedItemProperty().addListener((observableValue, contact, t1) -> selectedContact = listView.getSelectionModel().getSelectedItem());
        try {
            ResultSet result= sqlDatabase.getResult();
            while(result.next()) {
                contacts.add(new Contact(result.getString("name"),result.getString("surname"),result.getString("number"),result.getString("member")));
                listView.setItems(contacts);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}