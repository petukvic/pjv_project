package fit.pjv.app;

import java.sql.*;

public class SQLDatabase {
    private static ResultSet result;
    private static Statement statement;
    private static Connection connection;

    public SQLDatabase(String url) {
        try {
            connection = DriverManager.getConnection(url);
            statement = connection.createStatement();
            result = statement.executeQuery("SELECT * FROM contacts;");
        } catch (SQLException e) {
            System.out.println("Error: could not connect to database");
            e.printStackTrace();
        }
    }

    public ResultSet getResult() throws SQLException {
        String resultQuery = "SELECT * FROM contacts";
        PreparedStatement resultStatement = connection.prepareStatement(resultQuery);
        result = resultStatement.executeQuery();
        return result;
    }

    public void insertContactIntoDb(Contact contact) throws SQLException {
        String insertQuery = "INSERT INTO contacts (name,surname,number,member) VALUES (?,?,?,?)";
        PreparedStatement insertStatement = connection.prepareStatement(insertQuery);
        insertStatement.setString(1, contact.getName());
        insertStatement.setString(2, contact.getSurname());
        insertStatement.setString(3, contact.getPhoneNum());
        insertStatement.setString(4, contact.getMember());
        insertStatement.executeUpdate();
    }

    public void deleteContactFromDb(String number) throws SQLException {
        String deleteQuery = "DELETE FROM contacts WHERE number=" + number;
        PreparedStatement deleteStatement = connection.prepareStatement(deleteQuery);
        deleteStatement.executeUpdate();
    }
}
